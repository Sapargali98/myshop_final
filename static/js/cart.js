$(document).ready(function() {
    $('.add-to-cart').click(function(event) {
      event.preventDefault();
      var productId = $(this).data('product-id');
      var quantity = $('.quantity-input').val();
      $.ajax({
        type: 'POST',
        url: '{% url "cart:add_to_cart" productId %}',
        data: {'quantity': quantity},
        success: function(data) {
          if (data.success) {
            alert('The product has been successfully added to the cart.');
          } else {
            alert('Sorry, but the item is out of stock.');
          }
        },
        error: function(data) {
          alert('An error occurred while adding the product to the cart.');
        }
      });
    });
  });