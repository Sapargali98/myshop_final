document.addEventListener('DOMContentLoaded', function () {
    const loginButton = document.getElementById('loginButton');
    const loginModal = document.getElementById('loginModal');
    const closeButton = document.querySelector('.close');

    if (loginButton && loginModal && closeButton) {
        loginButton.addEventListener('click', function() {
            loginModal.style.display = 'block';
        });

        closeButton.addEventListener('click', function() {
            loginModal.style.display = 'none';
        });

        window.addEventListener('click', function(event) {
            if (event.target == loginModal) {
                loginModal.style.display = 'none';
            }
        });

    console.log('Korean Beauty website loaded'); 
  }});

document.addEventListener("DOMContentLoaded", function() {
    const messages = document.querySelectorAll('.message');
    messages.forEach(message => {
        setTimeout(() => {
            message.style.display = 'none';
        }, 5000); // Сообщения исчезают через 5 секунд
    });
});
$(document).ready(function() {
    $('#submitReview').click(function() {
        var rating = $('#rating').val();
        var comment = $('#comment').val();
        var csrfToken = $('input[name=csrfmiddlewaretoken]').val();
        var productId = '{{ product.pk }}'; // Получаем ID продукта

        var submitUrl = '/detail/' + productId + '/submit_review/';

        $.ajax({
            url: submitUrl,
            method: 'POST',
            data: {
                rating: rating,
                comment: comment,
                csrfmiddlewaretoken: csrfToken
            },
            success: function(response) {
                // Действия при успешной отправке, например, обновление страницы или вывод сообщения об успешной отправке
            },
            error: function(xhr, errmsg, err) {
                // Действия при ошибке отправки, например, вывод сообщения об ошибке
            }
        });
    });
});
