from django.db import models
from django.contrib.auth.models import User

class Order(models.Model):
    STATUS_CHOICES = (
        ('created', 'Создан'),
        ('confirmed', 'Подтверждён'),
        ('awaiting_payment', 'Ожидает оплаты'),
        ('paid', 'Оплачен'),
        ('cancelled', 'Отменён')
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    total_price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='created')
    is_paid = models.BooleanField(default=False)
    address = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return f'Order {self.pk} - {self.user.username} - {self.status}'

    def get_total_order_price(self):
        return sum(item.get_total_item_price() for item in self.items.all())

class OrderItem(models.Model):
    order = models.ForeignKey(Order, related_name='items', on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    product = models.ForeignKey('product.Product', on_delete=models.CASCADE,default=1)

    def get_total_item_price(self):
        return self.price * self.quantity
