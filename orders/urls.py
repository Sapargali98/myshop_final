from django.urls import path
from . import views

app_name = 'orders'

urlpatterns = [
path('create/', views.create_order, name='create_order'),
path('confirm/<int:order_id>/', views.confirm_order, name='confirm_order'),
path('awaiting_payment/<int:order_id>/', views.awaiting_payment, name='awaiting_payment'),
path('pay/<int:order_id>/', views.pay_order, name='pay_order'),
path('<int:order_id>/', views.order_detail, name='order_detail'),
path('<int:order_id>/cancel/', views.cancel_order, name='cancel_order'),
path('', views.order_history, name='order_history'),
path('thank_you/<int:order_id>',views.thank_you,name='thank_you'),
]