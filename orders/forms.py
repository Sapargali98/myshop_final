from django import forms
from .models import Order

class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['address']  

        
        widgets = {
            'address': forms.TextInput(attrs={'placeholder': 'Введите адрес доставки'}),
        }

    # Если нужно добавить кастомную логику валидации, вы можете использовать методы clean_<fieldname>()
    def clean_address(self):
        address = self.cleaned_data.get('address')
        if not address:
            raise forms.ValidationError("Пожалуйста, укажите адрес доставки.")
        return address