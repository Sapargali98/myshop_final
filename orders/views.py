from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from .models import Order, OrderItem
from cart.models import Cart
@login_required
def create_order(request):
    cart = request.user.cart if request.user.is_authenticated else Cart.objects.create()
    cart_items = cart.items.all()
    total_price = cart.total_price
    if request.method == 'POST':
        order = Order.objects.create(user=request.user, total_price=total_price)
        for cart_item in cart_items:
            order_item = OrderItem.objects.create(order=order, product=cart_item.product, price=cart_item.product.price, quantity=cart_item.quantity)
            cart_item.quantity -= 1
            if cart_item.quantity == 0:
                cart_item.delete()
        cart.save()
        return redirect('orders:order_detail', order_id=order.pk)
    return render(request, 'orders/create.html', {'cart_items': cart_items, 'total_price': total_price})
def get_order_or_404(order_id, user):
    return get_object_or_404(Order, id=order_id, user=user)
@login_required
def confirm_order(request, order_id):
    order = get_order_or_404(order_id, request.user)
    if request.method == 'POST':
        order.address = request.POST.get('address')
        order.status = 'confirmed'
        order.save()
        return redirect('orders:awaiting_payment', order_id=order.id)
    return render(request, 'orders/confirm_order.html', {'order': order})

@login_required
def awaiting_payment(request, order_id):
    order = get_order_or_404(order_id, request.user)
    if request.method == 'POST':
        order.status = 'awaiting_payment'
        order.save()
        return redirect('orders:pay_order', order_id=order.id)
    return render(request, 'orders/awaiting_payment.html', {'order': order})

@login_required
def pay_order(request, order_id):
    order = get_object_or_404(Order, id=order_id, user=request.user)
    if request.method == 'POST':
        order.is_paid = True
        order.status = 'paid'
        order.save()
        return redirect('orders:thank_you', order_id=order.id)
    return render(request, 'orders/pay_order.html', {'order': order})

@login_required
def cancel_order(request, order_id):
    order = get_order_or_404(order_id, request.user)
    if order.status == 'paid':
        messages.get_messages(request).used = True
        messages.error(request, "Заказ уже оплачен и не может быть отменён.")
        return redirect('orders:order_detail', order_id=order_id)
    order.status = 'cancelled'
    order.save()
    messages.success(request, "Заказ успешно отменён.")
    return redirect('orders:order_history')

@login_required
def order_history(request):
    orders = Order.objects.filter(user=request.user).order_by('-created_at')
    return render(request, 'orders/order_history.html', {'orders': orders})


@login_required
def order_detail(request, order_id):
    order = get_object_or_404(Order, id=order_id, user=request.user)

    # Очистка предыдущих сообщений
    storage = messages.get_messages(request)
    storage.used = True  # Отметьте все сообщения как использованные

    if request.method == 'POST' and 'confirm_address' in request.POST:
        address = request.POST.get('address')
        order.address = address
        order.status = 'confirmed'
        order.save()
        messages.success(request, 'Адрес подтвержден и заказ сохранен!')
        return redirect('orders:awaiting_payment', order_id=order.id)

    # Перенаправление в зависимости от статуса заказа
    if order.status == 'confirmed':
        return redirect('orders:awaiting_payment', order_id=order.id)
    elif order.status == 'awaiting_payment':
        return redirect('orders:pay_order', order_id=order.id)
    elif order.status == 'paid':
        return redirect('orders:thank_you', order_id=order.id)
    elif order.status == 'cancelled':
        return redirect('orders:cancel_order', order_id=order.id)

    # Отображение страницы деталей заказа
    return render(request, 'orders/order_detail.html', {'order': order})
@login_required
def thank_you(request, order_id):
    # Используйте get_object_or_404 для обработки случаев, когда заказ не найден
    order = get_object_or_404(Order, id=order_id)
    
    # Нет необходимости явно вызывать .all(), доступ к related_name 'items' предоставит необходимый QuerySet
    order_items = order.items.all() if hasattr(order, 'items') else []

    context = {
        'order': order,
        'order_items': order_items,  # Предоставляет данные о товарах в заказе
    }
    return render(request, 'orders/thank_you.html', context)
@login_required
def cancel_order(request, order_id):
    order = get_object_or_404(Order, id=order_id, user=request.user)

    if order.status == 'paid':
        messages.error(request, "Заказ уже оплачен и не может быть отменён.")
        return redirect('orders:order_detail', order_id=order_id)

    order.status = 'cancelled'
    order.save()
    messages.success(request, "Заказ успешно отменён.")
    return redirect('orders:order_history')