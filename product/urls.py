from django.urls import path
from . import views
app_name = 'product'

urlpatterns = [
    path('', views.product_list_view, name='product_list'),
    path('detail/<int:pk>/', views.product_detail, name='product_detail'),
    path('detail/<int:pk>/submit_review/', views.product_review_submit, name='product_review_submit'),
    path('form',views.product_create_view,name='product_form'),
    path('category/skincare/', views.skincare_products, name='skincare_products'),
    path('category/makeup/', views.makeup_products, name='makeup_products'),
    path('category/haircare/', views.haircare_products, name='haircare_products'),
    path('out_of_stock/', views.out_of_stock, name='out_of_stock'),
]