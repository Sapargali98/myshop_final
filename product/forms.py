from django import forms
from .models import Product, ProductReview

class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['name', 'description', 'price', 'in_stock', 'category','brand', 'image','quantity']

    error_messages = {
        'name': {
            'required': 'Please enter a name for the product.',
            'max_length': 'The name of the product cannot be longer than 255 characters.',
        },
        'description': {
            'required': 'Please enter a description for the product.',
        },
        'price': {
            'required': 'Please enter a price for the product.',
            'invalid': 'Please enter a valid price for the product.',
        },
        'in_stock': {
            'required': 'Please indicate whether the product is in stock.',
        },
        'category': {
            'required': 'Please select a category for the product.',
        },
        'brand':{
            'required':'Please select a brand for the product.',
        },
        'image': {
            'required': 'Please upload an image for the product.',
            'invalid': 'Please upload a valid image file.',
            'max_length': 'Image file size must be less than 2 MB.',
        },
        'quantity': {
            'required': 'Pleace enter a quantity of a product',
        },
    }

    def clean_price(self):
        price = self.cleaned_data['price']
        if price < 0:
            raise forms.ValidationError('Price must be a positive number.')
        return price

    def clean_image(self):
        image = self.cleaned_data['image']
        if image.size > 2 * 1024 * 1024:
            raise forms.ValidationError('Image file size must be less than 2 MB.')
        return image
class ProductReviewForm(forms.ModelForm):
    class Meta:
        model = ProductReview
        fields = ('rating', 'comment')

    def clean(self):
        cleaned_data = super().clean()
        rating = cleaned_data.get('rating')
        comment = cleaned_data.get('comment')

        if not rating or not comment:
            self.add_error('rating', 'Rating is required.')
            self.add_error('comment', 'Comment is required.')