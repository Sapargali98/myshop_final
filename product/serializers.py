from rest_framework import serializers
from .models import Product, ProductReview

class ProductReviewSerializer(serializers.ModelSerializer):
    user_id = serializers.ReadOnlyField(source='user.id') 
    class Meta:
        model = ProductReview
        fields = ['id', 'product', 'user_id', 'rating', 'comment', 'created_at', 'is_approved']
        read_only_fields = ['user_id', 'is_approved']  
class ProductSerializer(serializers.ModelSerializer):
    reviews = ProductReviewSerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = ['id', 'name', 'description', 'price', 'reviews']