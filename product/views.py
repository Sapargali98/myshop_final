from django.http import HttpResponseForbidden
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.db.models import Q
from django.urls import reverse
from django.core.paginator import Paginator,EmptyPage,PageNotAnInteger
from orders.models import OrderItem
from product.serializers import ProductSerializer,ProductReviewSerializer
from .models import  Product, ProductReview
from .forms import ProductForm, ProductReviewForm
from rest_framework import viewsets,generics
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import viewsets
from .permissions import  IsOwnerOrReadOnly, IsAdminOrReadOnly, IsAuthenticatedOrReadOnly
from django.http import JsonResponse

from rest_framework import viewsets, status
def home_view(request):
    return render(request, 'home.html')

def product_list_view(request):
    query = request.GET.get('q', '')
    sort_by = request.GET.get('sort_by', '')
    page_request = request.GET.get('page', 1)

    products = Product.objects.all().order_by('id')
    if query:
        products = products.filter(Q(name__icontains=query) | Q(description__icontains=query))
    if sort_by == 'price-low-high':
        products = products.order_by('price')
    elif sort_by == 'price-high-low':
        products = products.order_by('-price')

    paginator = Paginator(products, 3)
    try:
        products = paginator.page(page_request)
    except PageNotAnInteger:
        return redirect('?page=1')
    except EmptyPage:
        return redirect(f'?page={paginator.num_pages}')

    context = {
        'products': products, 
        'object_list': products,  
        'query': query,
        'sort_by': sort_by
    }

    return render(request, 'product/list.html', context)

def product_detail(request, pk):
    product = get_object_or_404(Product, pk=pk)
    reviews = ProductReview.objects.filter(product=product, is_approved=True)
    user_has_purchased = False
    new_review = None

    if request.user.is_authenticated:
        user_has_purchased = True

    review_form = ProductReviewForm()

    context = {
        'product': product,
        'reviews': reviews,
        'new_review': new_review,
        'review_form': review_form,
        'user_has_purchased': user_has_purchased
    }
    return render(request, 'product/detail.html', context)
def is_admin(user):
    return user.is_active and user.is_staff
@login_required
@user_passes_test(is_admin)
def product_create_view(request):
    if request.method == 'POST':
        form = ProductForm(request.POST, request.FILES)
        if form.is_valid():
            product = form.save(commit=False)
            product.user = request.user
            product.save()
            messages.success(request, 'Your product has been created.')
            return redirect(reverse('product:product_list'))
    else:
        form = ProductForm()

    context = {
        'form': form,
    }

    return render(request, 'product/form.html', context)

@login_required
@user_passes_test(is_admin)
def product_edit_view(request, pk):
    product = get_object_or_404(Product, pk=pk)

    if request.method == 'POST':
        form = ProductForm(request.POST, request.FILES, instance=product)
        if form.is_valid():
            product = form.save()
            messages.success(request, 'Your product has been updated.')
            return redirect(reverse('product:product_detail', args=[product.pk]))
    else:
        form = ProductForm(instance=product)

    context = {
        'form': form,
    }

    return render(request, 'product/form.html', context)

@login_required
@user_passes_test(is_admin)
def product_delete_view(request, pk):
    product = get_object_or_404(Product, pk=pk)

    if request.method == 'POST':
        product.delete()
        messages.success(request, 'Your product has been deleted.')
        return redirect(reverse('product:product_list'))

    context = {
        'product': product,
    }

    return render(request, 'products/product_confirm_delete.html', context)

def skincare_products(request):
    products = Product.objects.filter(category__name='Уход за лицом')

    query = request.GET.get('q')
    if query:
        products = products.filter(name__icontains=query)

    sort_by = request.GET.get('sort_by')
    if sort_by == 'price-low-high':
        products = products.order_by('price')
    elif sort_by == 'price-high-low':
        products = products.order_by('-price')


    paginator = Paginator(products, 3)  
    page_number = request.GET.get('page', 1)

    try:
        products_page = paginator.page(page_number)
    except PageNotAnInteger:
        # Если страница не является целым числом, доставить первую страницу
        products_page = paginator.page(1)
    except EmptyPage:
        # Если страница выходит за допустимый диапазон (например, 9999), доставить последнюю страницу
        products_page = paginator.page(paginator.num_pages)

    return render(request, 'category/skincare.html', {
        'products': products_page,  # передаем пагинированную страницу
        'query': query,
        'sort_by': sort_by
    })

def makeup_products(request):
    products = Product.objects.filter(category__name='Макияж')

    query = request.GET.get('q')
    if query:
        products = products.filter(name__icontains=query)

    sort_by = request.GET.get('sort_by')
    if sort_by == 'price-low-high':
        products = products.order_by('price')
    elif sort_by == 'price-high-low':
        products = products.order_by('-price')

    paginator = Paginator(products, 3)  
    page_number = request.GET.get('page', 1)

    try:
        products_page = paginator.page(page_number)
    except PageNotAnInteger:
        # Если страница не является целым числом, доставить первую страницу
        products_page = paginator.page(1)
    except EmptyPage:
        # Если страница выходит за допустимый диапазон (например, 9999), доставить последнюю страницу
        products_page = paginator.page(paginator.num_pages)

    return render(request, 'category/makeup.html', {
        'products': products_page,  # передаем пагинированную страницу
        'query': query,
        'sort_by': sort_by
    })

def haircare_products(request):
    products = Product.objects.filter(category__name='Уход за волосами')

    query = request.GET.get('q')
    if query:
        products = products.filter(name__icontains=query)

    sort_by = request.GET.get('sort_by')
    if sort_by == 'price-low-high':
        products = products.order_by('price')
    elif sort_by == 'price-high-low':
        products = products.order_by('-price')

    paginator = Paginator(products, 3)  
    page_number = request.GET.get('page', 1)

    try:
        products_page = paginator.page(page_number)
    except PageNotAnInteger:
        # Если страница не является целым числом, доставить первую страницу
        products_page = paginator.page(1)
    except EmptyPage:
        # Если страница выходит за допустимый диапазон (например, 9999), доставить последнюю страницу
        products_page = paginator.page(paginator.num_pages)

    return render(request, 'category/haircare.html', {
        'products': products_page,  # передаем пагинированную страницу
        'query': query,
        'sort_by': sort_by
    })
@login_required
@user_passes_test(is_admin)
def out_of_stock(request):
    products = Product.objects.filter(quantity=0)
    return render(request, 'product/out_of_stock.html', {'products': products})
class ProductDetailView(generics.RetrieveAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
class ProductReviewViewSet(viewsets.ModelViewSet):
    queryset = ProductReview.objects.all()
    serializer_class = ProductReviewSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly, IsAdminOrReadOnly]

    def perform_create(self, serializer):
        # Присвоение текущего пользователя в качестве автора отзыва
        serializer.save(user=self.request.user)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        # Проверка прав пользователя перед удалением
        self.check_object_permissions(self.request, instance)
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)
def product_review_submit(request, pk):
    if request.method == 'POST' and request.user.is_authenticated:
        review_form = ProductReviewForm(request.POST)
        if review_form.is_valid():
            review = review_form.save(commit=False)
            review.user = request.user
            review.product = get_object_or_404(Product, pk=pk)
            review.save()
            return JsonResponse({'success': True})
        else:
            return JsonResponse({'success': False, 'errors': review_form.errors}, status=400)
    else:
        return JsonResponse({'success': False, 'errors': 'Authentication required'}, status=401)