from django.db import models
from orders.models import Order
class Brand(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name
class Category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Product(models.Model):
        name = models.CharField(max_length=255)
        description = models.TextField()
        price = models.DecimalField(max_digits=10, decimal_places=2)
        in_stock = models.BooleanField(default=True)
        category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)
        brand = models.ForeignKey(Brand, on_delete=models.SET_NULL, null=True, related_name='products')
        image = models.ImageField(upload_to='product/')
        quantity = models.PositiveIntegerField(default=0) 

def __str__(self):
        return self.name

class ProductReview(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='reviews')
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    rating = models.IntegerField(choices=[(i, i) for i in range(1, 6)])
    comment = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    order = models.ForeignKey(Order, on_delete=models.CASCADE ,null=True, related_name='reviews')
    is_approved = models.BooleanField(default=False) 

def __str__(self):
    order_status = f", Order status: {self.order.status}" if self.order else ""
    return f'{self.user.username} - {self.product.name} - Rating: {self.rating}{order_status}'