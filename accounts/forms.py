from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm,UserChangeForm
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
# from cart.views import User
from .models import Profile
class LoginForm(AuthenticationForm):
    username = forms.CharField(
        label=_('Имя пользователя'),
        required=True, 
        widget=forms.TextInput(attrs={'placeholder': _('Введите ваше имя пользователя')})
    )
    password = forms.CharField(
        label=_('Пароль'),
        widget=forms.PasswordInput(attrs={'placeholder': _('Введите ваш пароль')}),
        required=True  
    )
class MyUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2'
        ]
        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'password1': forms.PasswordInput(attrs={'class': 'form-control'}),
            'password2': forms.PasswordInput(attrs={'class':'form-control'}),
        }
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].label = _('Имя пользователя')
        self.fields['first_name'].label = _('Имя')
        self.fields['last_name'].label = _('Фамилия')
        self.fields['email'].label = _('Электронная почта')
        self.fields['password1'].label = _('Пароль')
        self.fields['password2'].label = _('Подтверждение пароля')
        

class PasswordChangeForm(forms.ModelForm):
    password = forms.CharField(
        label='Новый пароль', 
        strip=False, 
        widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password_confirm = forms.CharField(
        label='Подтвердите пароль', 
        strip=False, 
        widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    old_password = forms.CharField(
        label='Старый пароль', 
        strip=False, 
        widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    def clean_password_confirm(self):
        password = self.cleaned_data.get('password')
        password_confirm = self.cleaned_data.get('password_confirm')

        if password and password_confirm and password != password_confirm:
            raise forms.ValidationError('Пароли разные!')
        return password_confirm
    
    def clean_old_password(self):
        old_password = self.cleaned_data.get('old_password')

        if not self.instance.check_password(old_password):
            raise forms.ValidationError('Старый пароль неверный!')
        return old_password
    
    def save(self, commit=True):
        user = self.instance
        user.set_password(self.cleaned_data.get('password'))
        if commit:
            user.save()
        return user
    
    class Meta:
        model = get_user_model()
        fields = ['old_password', 'password', 'password_confirm']

class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['position', 'phone', 'avatar']
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['position'].label = _('Позиция')
        self.fields['phone'].label = _('Телефон')
        self.fields['avatar'].label = _('Аватарка')

class UserForm(UserChangeForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].label = _('Имя')
        self.fields['last_name'].label = _('Фамилия')
        self.fields['email'].label = _('Электронная почта')