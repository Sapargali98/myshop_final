from django.urls import path
from accounts.views import login_view,logout_view,profile_view,create_profile,edit_profile
from .views import UserPasswordChangeView,RegisterView
from django.conf import settings
from django.conf.urls.static import static
app_name = 'accounts'
urlpatterns = [
    path('profile/', profile_view, name='profile'),
    path('profile/create', create_profile, name='create_profile'),
    path('profile/edit', edit_profile, name='edit_profile'),
    path('password_change/', UserPasswordChangeView.as_view(), name='change_password'),
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('register/', RegisterView.as_view(), name='register'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
