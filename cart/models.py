from django.db import models
from django.conf import settings
from product.models import Product  # assuming this is where your Product model is defined

class Cart(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    session_key = models.CharField(max_length=255, null=True, blank=True)  

    def __str__(self):
        return f"Cart of {self.user.username if self.user else 'Anonymous'}"

    @property
    def total_price(self):
        return sum(item.subtotal for item in self.items.all())

    @property
    def total_items(self):
        return sum(item.quantity for item in self.items.all())

    def remove_item(self, product_id):
        try:
            cart_item = self.items.get(product_id=product_id)
            cart_item.delete()
            return True  # Successfully removed
        except CartItem.DoesNotExist:
            return False  # Product not found in cart

class CartItem(models.Model):
    cart = models.ForeignKey(Cart, related_name='items', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)

    def __str__(self):
        return f"{self.quantity} of {self.product.name}"

    @property
    def subtotal(self):
        return self.product.price * self.quantity
