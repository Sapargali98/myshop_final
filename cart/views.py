from django.shortcuts import render, redirect, get_object_or_404
from .models import Cart, CartItem
from product.models import Product
from django import forms
from django.shortcuts import redirect, render, get_object_or_404
from django.contrib import messages

def get_or_create_cart(request):
    if request.user.is_authenticated:
        cart, _ = Cart.objects.get_or_create(user=request.user)
    else:
        session_key = request.session.session_key
        if not session_key:
            request.session.create()
            session_key = request.session.session_key
        cart, _ = Cart.objects.get_or_create(session_key=session_key)
    return cart
    
def cart_detail(request):
    cart = get_or_create_cart(request)  # Используем функцию для получения или создания корзины
    cart_items = cart.items.all()
    total_price = sum(item.product.price * item.quantity for item in cart_items)  # Вычисляем общую стоимость

    return render(request, 'cart/detail.html', {
        'cart_items': cart_items,
        'total_price': total_price
    })
def add_to_cart(request, product_id):
    product = get_object_or_404(Product, id=product_id)
    cart = get_or_create_cart(request)
    cart_item, created = CartItem.objects.get_or_create(cart=cart, product=product)
    if not created:
        cart_item.quantity += 1
    else:
        cart_item.quantity = 1
    cart_item.save()
    return redirect('cart:cart_detail')

class CartItemForm(forms.Form):
    quantity = forms.IntegerField(min_value=0)  # Устанавливаем минимальное значение

def update_cart(request, cart_item_id):
    cart_item = get_object_or_404(CartItem, id=cart_item_id)
    if request.method == 'POST':
        form = CartItemForm(request.POST)
        if form.is_valid():
            quantity = form.cleaned_data['quantity']
            if quantity > 0:
                cart_item.quantity = quantity
                cart_item.save()
                messages.success(request, "Количество товара успешно обновлено.")
            else:
                cart_item.delete()
                messages.success(request, "Товар удален из корзины.")
            return redirect('cart:cart_detail')
        else:
            messages.error(request, "Введено некорректное количество.")
    else:
        form = CartItemForm(initial={'quantity': cart_item.quantity})

    return render(request, 'cart/update_cart.html', {'cart_item': cart_item, 'form': form})
def delete_from_cart(request, cart_item_id):
    cart_item = get_object_or_404(CartItem, id=cart_item_id)
    cart_item.delete()
    return redirect('cart:cart_detail')
