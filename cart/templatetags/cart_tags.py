from django import template

register = template.Library()

@register.filter(name='mul')
def mul(value, arg):
    """Умножает value на arg."""
    try:
        return value * arg
    except (ValueError, TypeError):
        return ''

@register.filter
def sum(cart_items, attribute):
    """Суммирует атрибут заданных объектов в списке."""
    return sum(getattr(item, attribute) for item in cart_items if hasattr(item, attribute))